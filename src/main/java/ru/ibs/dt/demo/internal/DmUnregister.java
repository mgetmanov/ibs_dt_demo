package ru.ibs.dt.demo.internal;

public interface DmUnregister {
    void unregisterJdbc(String url);
}
