package ru.ibs.dt.demo.internal;

public interface DmWrapper {
    JdbcWrapper jdbc(String url);

    KafkaWrapper kafka(String url);
}
