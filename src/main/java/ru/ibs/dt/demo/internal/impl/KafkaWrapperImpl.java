package ru.ibs.dt.demo.internal.impl;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongSerializer;
import ru.ibs.dt.demo.internal.KafkaWrapper;

import java.util.Collections;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class KafkaWrapperImpl implements KafkaWrapper<byte[]> {
    private final String url;

    public KafkaWrapperImpl(String url) throws Exception {
        this.url = url;
    }

    @Override
    public void createTopic(String topicName) throws Exception {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, url);
        AdminClient kafkaAdminClient = KafkaAdminClient.create(properties);
        CreateTopicsResult result = kafkaAdminClient.createTopics(
                Stream.of(topicName).map(
                        name -> new NewTopic(name, 3, (short) 1)
                ).collect(Collectors.toList())
        );
        result.all().get();
    }

    @Override
    public void removeTopic(String topicName) throws Exception {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, url);
        AdminClient kafkaAdminClient = KafkaAdminClient.create(properties);
        DeleteTopicsResult result = kafkaAdminClient.deleteTopics(Collections.singleton(topicName));
        result.all().get();
    }

    @Override
    public void produceMessage(String topic, Map<String, Object> msg) {

    }

    @Override
    public void consumeMessage(String topic, Callable consumer) {

    }

    @Override
    public void close() {

    }

    private Producer<Long, String> createProducer(String ) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, url);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        return new KafkaProducer<>(props);
    }
}
