package ru.ibs.dt.demo.internal.impl;

import ru.ibs.dt.demo.internal.DmWrapper;
import ru.ibs.dt.demo.internal.JdbcWrapper;
import ru.ibs.dt.demo.internal.KafkaWrapper;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DmWrapperImpl implements DmWrapper {
    private final Map<String, JdbcWrapper> jdbcMap = new ConcurrentHashMap();
    private final Map<String, KafkaWrapper> kafkaMap = new ConcurrentHashMap();

    @Override
    public JdbcWrapper jdbc(String url) {
        if (!jdbcMap.containsKey(url)) {
            JdbcWrapper wrapper = new JdbcWrapperImpl(url);
            if (!jdbcMap.putIfAbsent(url, wrapper).equals(wrapper)) {
                wrapper.close();
            }
        }
        return jdbcMap.get(url);
    }

    @Override
    public KafkaWrapper kafka(String url) {
        if (!kafkaMap.containsKey(url)) {
            KafkaWrapper wrapper = new KafkaWrapperImpl(url);
            if (!kafkaMap.putIfAbsent(url, wrapper).equals(wrapper)) {
                wrapper.close();
            }
        }
        return kafkaMap.get(url);
    }
}
