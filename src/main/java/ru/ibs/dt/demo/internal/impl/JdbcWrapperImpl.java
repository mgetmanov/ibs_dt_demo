package ru.ibs.dt.demo.internal.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.arenadata.dtm.jdbc.DtmDriver;
import ru.ibs.dt.demo.internal.DmUnregister;
import ru.ibs.dt.demo.internal.JdbcWrapper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcWrapperImpl implements JdbcWrapper {
    private static final Logger log = LoggerFactory.getLogger(JdbcWrapperImpl.class);

    static {
        try {
            DriverManager.registerDriver(new DtmDriver());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private final Connection connection;
    private final DmUnregister dmUnregister;
    private final String url;

    public JdbcWrapperImpl(DmUnregister dmUnregister, String url) throws Exception {
        this.url = url;
        connection = DriverManager.getConnection(url);
        this.dmUnregister = dmUnregister;
    }

    @Override
    public void beginDelta() throws SQLException {
        connection.createStatement().execute("BEGIN DELTA;");
    }

    @Override
    public void close() {
        dmUnregister.unregisterJdbc(url);
        try {
            connection.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void commitDelta() throws SQLException {
        connection.createStatement().execute("COMMIT DELTA;");
    }

    @Override
    public void createDb(String dbName) throws SQLException {
        log.info("connected");
        connection.createStatement().execute("CREATE DATABASE " + dbName + ";");
    }

    @Override
    public void createTable(String sql) throws SQLException {
        connection.createStatement().execute(sql);
    }

    @Override
    public void dropTable(String name) throws SQLException {
        connection.createStatement().execute("DROP TABLE " + name + ";");
    }

    @Override
    public void dropDownLoadTable(String name) throws SQLException {
        connection.createStatement().execute("DROP DOWNLOAD EXTERNAL TABLE " + name + ";");
    }

    @Override
    public void insertOperation(String sql) throws SQLException {
        connection.createStatement().execute(sql);
    }

    @Override
    public ResultSet select(String sql) throws SQLException {
        return connection.createStatement().executeQuery(sql);
    }
}
