package ru.ibs.dt.demo.internal;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface JdbcWrapper extends AutoCloseable {
    void alterTable(String sql);

    void alterView(String sql);

    void beginDelta() throws SQLException;

    void close();

    void commitDelta() throws SQLException;

    void createDb(String dbName) throws SQLException;

    void createIndex(String sql);

    void createTable(String sql) throws SQLException;

    void createView(String sql);

    void dropDb(String name);

    void dropTable(String name) throws SQLException;

    void dropView(String name);

    void dropDownLoadTable(String name) throws SQLException;

    void insertOperation(String sql) throws SQLException;

    ResultSet select(String sql) throws SQLException;

    void selectOfSystemSchema(String sql);

    void useDb(String dbName);
}
