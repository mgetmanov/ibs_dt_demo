package ru.ibs.dt.demo.internal;

import java.util.Map;
import java.util.concurrent.Callable;

public interface KafkaWrapper<K> extends AutoCloseable {
    void createTopic(String topicName) throws Exception;

    void removeTopic(String topicName) throws Exception;

    void produceMessage(String topic, Map<String,Object> msg);

    void consumeMessage(String topic, Callable<K> consumer);
    void close();
}
